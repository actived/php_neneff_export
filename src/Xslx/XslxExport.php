<?php

namespace Neneff\Export\Xslx;

use Neneff\Export\AbstractExport;
use PhpOffice\PhpSpreadsheet\Cell\Coordinate;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Style\Fill;
use PhpOffice\PhpSpreadsheet\Worksheet\Worksheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;


class XslxExport extends AbstractExport
{
    /** @var string */
    protected $extension = 'xslx';

    /** @var  Xlsx */
    protected $writer;

    /** @var  Spreadsheet */
    protected $spreadsheet;

    /** @var  Worksheet */
    protected $worksheet;


    /**
     * AbstractExport constructor.
     * @param  String $filename
     * @throws
     */
    public function __construct($filename = 'new_file')
    {
        parent::__construct($filename);
    }

    /**
     * @throws
     */
    protected function onDocumentWriteStart()
    {
        $this->spreadsheet = new Spreadsheet();
        $this->writer      = new Xlsx($this->spreadsheet);
        $this->worksheet   = $this->spreadsheet->setActiveSheetIndex(0);
    }

    /**
     * @param integer $rowIndex
     * @param array   $row
     * @throws
     */
    protected function onRowIsWritten($rowIndex, $row)
    {
        $rowStyle = $this->generateRowStyle($rowIndex, $row);
        if($rowStyle !== null) {
            $this->worksheet->getStyle("{$rowIndex}:{$rowIndex}")->applyFromArray($rowStyle);
        }
    }

    /**
     * @param integer $colIndex
     * @param integer $rowIndex
     * @param mixed   $cell
     */
    protected function onCellIsWritten($colIndex, $rowIndex, $cell)
    {
        $cellStyle = $this->generateCellStyle($colIndex, $rowIndex, $cell);
        if($cellStyle !== null) {
            $this->worksheet->getStyleByColumnAndRow($colIndex, $rowIndex)->applyFromArray($cellStyle);
        }
    }

    /**
     * @param $row
     */
    final protected function writeRow($row) {}

    /**
     * @param $rows
     * @return void
     * @throws
     */
    protected function onDocumentWriteEnd($rows)
    {
        $this->worksheet->fromArray($rows);

        $first = Coordinate::columnIndexFromString("A");
        $last  = Coordinate::columnIndexFromString($this->worksheet->getHighestColumn());

        for($i=$first; $i<=$last; $i++)
        {
            $column = Coordinate::stringFromColumnIndex($i);
            $this->worksheet->getColumnDimension($column)->setAutoSize(true);
        }
    }


    /** ********* Styling ************* **/

    /**
     * return an array with the style description or NULL
     * @param  Integer $rowIndex
     * @param  array   $row
     * @return array|null
     */
    protected function generateRowStyle($rowIndex, $row)
    {
        if ($rowIndex % 2 === 0)
        {
            return [
                'fill' => [
                    'fillType' => Fill::FILL_SOLID,
                    'color'    => ['rgb'=>'F2F2F2']
                ]
            ];
        }
        else {
            return null;
        }
    }

    /**
     * return an array with the style description or NULL
     * @param Integer $rowIndex
     * @param Integer $colIndex
     * @param Mixed   $cell
     * @return array | null
     */
    protected function generateCellStyle($colIndex, $rowIndex, $cell)
    {
        return null;
    }


    /** ********* DOWNLOAD / SAVE FIlE ************* **/

    /**
     * @throws
     */
    public function downloadAsStream()
    {
        if(!$this->writer) {
            throw new \Exception('Copy could not be performed. The file has not been generated before');
        }

        // reset the file pointer to the start of the file
        header('Content-type: text/csv'); // ; charset=UTF-8' ??
        header('Content-Disposition: attachment; filename="'.$this->getFilename().'.'.$this->getExtension().'"');
        header('Pragma: no-cache');

        $this->writer->save('php://output');
        exit(0);
    }

    /**
     * Save the file to the directory with current filename and return the full access path
     * @param  String $directory
     * @return String
     * @throws
     */
    public function saveToPath($directory = '.')
    {
        if(!$this->writer) {
            throw new \Exception('Copy could not be performed. The file has not been generated before');
        }

        $destPath = realpath($directory).DIRECTORY_SEPARATOR.$this->getFilename().'.'.$this->getExtension();
        $this->writer->save($destPath);

        return $destPath;
    }
}