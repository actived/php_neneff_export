<?php

namespace Neneff\Export;


/**
 * Class AbstractExport for reto compatibility
 * @package Neneff\Export\Excel
 */
abstract class AbstractExport
{

    /*** @var string ***/
    protected $fileName;

    /*** @var string ***/
    protected $extension;


    /**
     * AbstractExport constructor.
     * @param  String $filename
     * @throws
     */
    public function __construct($filename = 'new_file')
    {
        $this->setFilename($filename);
    }

    /**
     * Write generate the first line to be included in the document
     * @return mixed
     */
    protected function generateHeader()
    {
        return [];
    }

    /**
     * Write generate the data lines to be added to the document
     * @return array
     */
    protected function generateRows()
    {
        return [];
    }


    /** ********* MAIN LOOP ************* **/

    /**
     * Write a row in $_workbook active sheet
     * @param  array $row
     * @return array
     */
    protected function processRow($row)
    {
        return $row;
    }

    /**
     * Generate the file with the data generated for this export and return the rows as an array
     * @return array
     * @throws
     */
    public function generateFile()
    {
        // -- prepare cells
        $headers = $this->generateHeader();
        $rows    = array_map(function($item) {
            return $this->processRow($item);
        }, $this->generateRows());

        // -- all cells merged
        $cells = array_merge($headers, $rows);

        $this->onDocumentWriteStart();
        // -- generate rows styles
        if(count($cells) > 0)
        {
            foreach($cells as $rowIndex => $row)
            {
                $this->onRowIsWritten($rowIndex, $row);

                // -- Style for cells
                foreach($row as $colIndex => $cell)
                {
                    $this->onCellIsWritten($colIndex, $rowIndex, $cell);
                }
                $this->writeRow($row);
            }
        }
        $this->onDocumentWriteEnd($rows);

        return $cells;
    }

    /**
     * @param $row
     */
    abstract protected function writeRow($row);



    /** ********* EVENTS ************* **/

    /**
     * @param integer $rowIndex
     * @param array   $row
     */
    protected function onRowIsWritten($rowIndex, $row) {}

    /**
     * @param integer $colIndex
     * @param integer $rowIndex
     * @param mixed   $cell
     */
    protected function onCellIsWritten($colIndex, $rowIndex, $cell) {}

    /**
     * @return void
     */
    protected function onDocumentWriteStart() {}

    /**
     * @param $rows
     * @return void
     */
    protected function onDocumentWriteEnd($rows) {}


    /** ********* FILENAME ************* **/

    /**
     * @return string
     */
    public function getExtension()
    {
        return $this->extension;
    }

    /**
     * @param $filename
     */
    public function setFilename($filename)
    {
        $this->fileName = $filename;
    }

    /**
     * @return string
     */
    public function getFilename()
    {
        if($this->fileName !== null) {
            return $this->fileName;
        }
        else {
            return 'new_file';
        }
    }


    /** ********* DOWNLOAD / SAVE FIlE ************* **/
    /**
     * @throws
     */
    abstract public function downloadAsStream();

    /**
     * Save the file to the directory with current filename and return the full access path
     * @param  String $directory
     * @return String
     * @throws
     */
    abstract public function saveToPath($directory = '');

}