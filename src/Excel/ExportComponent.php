<?php

namespace Neneff\Export\Excel;

use PhpOffice\PhpSpreadsheet\Cell\Coordinate;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Style\Fill;
use PhpOffice\PhpSpreadsheet\Worksheet\Worksheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

/**
 * @Deprecated
 * @package Neneff\Export\Excel
 */
abstract class ExportComponent extends AbstractExport
{


    /**
     * @return Worksheet
     */
    public function getWorksheet()
    {
        return $this->_worksheet;
    }
}