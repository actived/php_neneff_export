<?php

namespace Neneff\Export\Excel;

use PhpOffice\PhpSpreadsheet\Cell\Coordinate;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Style\Fill;
use PhpOffice\PhpSpreadsheet\Worksheet\Worksheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

/**
 * @Deprecated
 * @package Neneff\Export\Excel
 */
abstract class AbstractExport
{
    /*** @var string ***/
    private $_fileName;

    /*** @var Spreadsheet ***/
    private $_spreadsheet;

    /*** @var Worksheet ***/
    protected $_worksheet;

    /** @var  Xlsx */
    private $_writer;

    /**
     * AbstractExport constructor.
     * @param  String $filename
     * @throws
     */
    public function __construct($filename = null)
    {
        $this->_fileName = $filename;

        $this->_spreadsheet = new Spreadsheet();
        $this->_writer      = new Xlsx($this->_spreadsheet);
        $this->_worksheet   = $this->_spreadsheet->setActiveSheetIndex(0);
    }

    /**
     * Generate the file with the data generated for this export and return the rows as an array
     * @return array
     * @throws
     */
    public function generateFile()
    {
        // -- prepare cells
        $data    = $this->_prepareExport();
        $headers = $this->_generateHeader();
        $rows    = array_map(function($item) {
            return $this->_generateRow($item);
        }, $data);

        // -- all cells merged
        $cells = array_merge($headers, $rows);

        // -- generate rows styles
        if(count($cells) > 0)
        {
            foreach($cells as $rowIndex => $row)
            {
                // -- Style for rows
                $rowStyle =  $this->_generateRowStyle($rowIndex, $row);
                if($rowStyle !== null)
                {
                    $this->_worksheet->getStyle("{$rowIndex}:{$rowIndex}")->applyFromArray($rowStyle);
                }

                // -- Style for cells
                foreach($row as $colIndex => $cell)
                {
                    // -- var_dump($rowIndex); var_dump($colIndex); var_dump($cell);
                    $cellStyle = $this->_generateCellStyle($colIndex, $rowIndex, $cell);
                    if($cellStyle !== null)
                    {
                        $this->_worksheet->getStyleByColumnAndRow($colIndex, $rowIndex)->applyFromArray($cellStyle);
                    }
                }
            }
        }
        $this->_worksheet->fromArray($cells);
        $this->_applyGeneralStyle($this->_worksheet);

        return $cells;
    }

    /**
     * Use to fetch data
     * @return array
     */
    abstract protected function _prepareExport();

    /**
     * Write $_headers in $_workbook active sheet
     * @return mixed
     */
    protected function _generateHeader()
    {
        return [];
    }

    /**
     * Write a row in $_workbook active sheet
     * @param  array $row
     * @return mixed
     */
    protected function _generateRow($row)
    {
        return $row;
    }

    /**
     * return an array with the style description or NULL
     * @param  Integer $rowIndex
     * @param  array   $row
     * @return array|null
     */
    protected function _generateRowStyle($rowIndex, $row)
    {
        if ($rowIndex % 2 === 0)
        {
            return [
                'fill' => [
                    'fillType' => Fill::FILL_SOLID,
                    'color'    => ['rgb'=>'F2F2F2']
                ]
            ];
        }
        else {
            return null;
        }
    }

    /**
     * return an array with the style description or NULL
     * @param Integer $rowIndex
     * @param Integer $colIndex
     * @param Mixed   $cell
     * @return array | null
     */
    protected function _generateCellStyle($colIndex, $rowIndex, $cell)
    {
        return null;
    }

    /**
     * @param Worksheet $worksheet
     * @throws
     */
    protected function _applyGeneralStyle($worksheet)
    {
        $first = Coordinate::columnIndexFromString("A");
        $last  = Coordinate::columnIndexFromString($worksheet->getHighestColumn());

        for($i=$first; $i<=$last; $i++)
        {
            $column = Coordinate::stringFromColumnIndex($i);
            $worksheet->getColumnDimension($column)->setAutoSize(true);
        }
    }

    /**
     * @param $filename
     */
    public function setFilename($filename)
    {
        $this->_fileName = $filename;
    }

    /**
     * @return string
     */
    public function getFilename()
    {
        if($this->_fileName !== null) {
            return $this->_fileName;
        }
        else {
            return 'temp'; // -- generate with the current class + datetime
        }
    }

    /**
     * @throws
     */
    public function downloadAsStream()
    {
        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment; filename="'.$this->getFilename().'.xlsx"');
        header('Pragma: no-cache');

        $this->_writer->save('php://output');
        exit(0);
    }

    /**
     * Save the file to the directory with current filename and return the full access path
     * @param  String $directory
     * @return String
     * @throws
     */
    public function saveToPath($directory = '')
    {
        if(!file_exists($directory)) {
            throw new \Exception("The directory \"{$directory}\" does not exists. You should have created it before you can use it to save data.");
        }

        $directory = realpath($directory).DIRECTORY_SEPARATOR;
        $fullPath  = $directory.$this->getFilename().'.xlsx';
        $this->_writer->save($fullPath);

        return $fullPath;
    }

}