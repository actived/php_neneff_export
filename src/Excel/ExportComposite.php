<?php

namespace Neneff\Export\Excel;

use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Worksheet\Worksheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

/**
 * @Deprecated
 * @package Neneff\Export\Excel
 */
class ExportComposite
{
    /*** @var string ***/
    private $_fileName;

    /*** @var Spreadsheet ***/
    private $_spreadsheet;

    /** @var  Xlsx */
    private $_writer;

    /**
     * AbstractExport constructor.
     * @param  String $filename
     * @throws
     */
    public function __construct($filename = null)
    {
        $this->_fileName    = $filename;
        $this->_spreadsheet = new Spreadsheet();
        $this->_writer      = new Xlsx($this->_spreadsheet);

        $this->_spreadsheet->removeSheetByIndex(0);
    }

    /**
     * @param String    $name
     * @param Worksheet $worksheet
     * @throws
     */
    public function addWorksheet($name, Worksheet $worksheet)
    {
        $worksheet->setTitle($name);

        $this->_spreadsheet->addExternalSheet($worksheet);
    }

    /**
     * @param $filename
     */
    public function setFilename($filename)
    {
        $this->_fileName = $filename;
    }

    /**
     * @return string
     */
    public function getFilename()
    {
        if($this->_fileName !== null) {
            return $this->_fileName;
        }
        else {
            return 'temp'; // -- generate with the current class + datetime
        }
    }

    /**
     * @throws
     */
    public function downloadAsStream()
    {
        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment; filename="'.$this->getFilename().'.xlsx"');
        header('Pragma: no-cache');

        $this->_writer->save('php://output');
        exit(0);
    }

    /**
     * Save the file to the directory with current filename and return the full access path
     * @param  String $directory
     * @return String
     * @throws
     */
    public function saveToPath($directory = '')
    {
        if(!file_exists($directory)) {
            throw new \Exception("The directory \"{$directory}\" does not exists. You should have created it before you can use it to save data.");
        }

        $directory = realpath($directory).DIRECTORY_SEPARATOR;
        $fullPath  = $directory.$this->getFilename().'.xlsx';
        $this->_writer->save($fullPath);

        return $fullPath;
    }

}