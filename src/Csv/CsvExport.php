<?php

namespace Neneff\Export\Csv;
use Neneff\Export\AbstractExport;


class CsvExport extends AbstractExport
{
    /** @var string */
    protected $extension = 'csv';

    /** @var string */
    protected $cellDataEnclosure = '"';

    /** @var string */
    protected $cellDelimiter = "\t";

    /** @var resource */
    protected $file = null;


    /**
     * AbstractExport constructor.
     * @param  String $filename
     * @throws
     */
    public function __construct($filename = 'new_file')
    {
        parent::__construct($filename);
    }

    /**
     * @throws
     */
    protected function onDocumentWriteStart()
    {
        $this->file = tmpfile();

        if(!$this->file) {
            throw new \Exception('Impossible to generate CSV the file to be created');
        }
    }


    /**
     * @param array $row
     * @inheritdoc
     */
    protected function writeRow($row)
    {
        fputcsv($this->file, $row, $this->cellDelimiter, $this->cellDataEnclosure);
    }


    /** ********* DOWNLOAD / SAVE FIlE ************* **/

    /**
     * @throws
     */
    public function downloadAsStream()
    {
        if(!$this->file) {
            throw new \Exception('Copy could not be performed. The file has not been generated before');
        }

        // reset the file pointer to the start of the file
        header('Content-type: text/csv'); // ; charset=UTF-8' ??
        header('Content-Disposition: attachment; filename="'.$this->getFilename().'.'.$this->getExtension().'"');
        header('Pragma: no-cache');

        fseek($this->file, 0);
        fpassthru($this->file);
        fclose($this->file);

        exit(0);
    }

    /**
     * Save the file to the directory with current filename and return the full access path
     * @param  String $directory
     * @return String
     * @throws
     */
    public function saveToPath($directory = '.')
    {
        if(!$this->file) {
            throw new \Exception('Copy could not be performed. The file has not been generated before');
        }

        $destPath = realpath($directory).DIRECTORY_SEPARATOR.$this->getFilename().'.'.$this->getExtension();
        $tmpPath  = stream_get_meta_data($this->file)['uri'];
        $isCopied = copy($tmpPath, $destPath);

        if(!$isCopied) {
            throw new \Exception("Unable to copy the file");
        }
        return $destPath;
    }

}